from flask import Flask, request, jsonify
from google.cloud import speech
from pydub import AudioSegment
from google.cloud.speech import types
from google.cloud.speech import enums

app = Flask(__name__)
# app.config["DEBUG"] = True

client = speech.SpeechClient()

# some defaults and allowed values
supported_language_codes = ['en-US', 'nl-NL']
default_language_code = 'en-US'
supported_formats = ['ogg', 'wav']

# HTTP code variables
OK = 200
BAD_REQUEST = 400
UNSUPPORTED_MEDIA_TYPE = 415
INTERNAL_SERVER_ERROR = 500


@app.route('/transcribe', methods=['POST'])
def transcribe():
    try:

        # get the content type from the incoming request
        content_type = str(request.headers.get("Content-Type")).split(";")[0]

        # if it is not of type multipart/form-data, send bad request
        if content_type != "multipart/form-data":
            return jsonify({
                "status": "fail",
                "data": {
                    "error": "This API request only supports multipart/form-data file uploads, "
                             "the content of the request was of type " + content_type,
                    "form_keys": {
                        "required": {
                            "audio_file": "<your_audio_file>",
                        },
                        "optional": {
                            "language_code": "<your_language_code>",
                        }
                    },
                    "supported_language_codes": supported_language_codes,
                    "default_language_code": default_language_code
                }
            }), BAD_REQUEST

        # get a possible language code from the request
        language_code = request.values.get('language_code', default_language_code)

        # if not supported, return bad request
        if language_code not in supported_language_codes:
            return jsonify({
                "status": "fail",
                "data": {
                    "error": "The provided language code was not supported.",
                    "supported_language_codes": supported_language_codes,
                    "default_language_code": default_language_code
                }
            }), BAD_REQUEST

        # retrieve audio file from request
        audio_file = request.files.get('audio_file', None)

        # if no file was present under audio_file key, return bad request
        if audio_file is None:
            return jsonify({
                "status": "fail",
                "data": {
                    "error": "No file was present in the request under the expected key.",
                    "key_name": "audio_file"
                }
            }), BAD_REQUEST

        # extract filename
        filename = str(audio_file.filename)

        # split into parts
        filename_parts = filename.split('.')

        # send BAD_REQUEST when the filename was not in the format *.*
        if len(filename_parts) <= 1:
            return jsonify({
                "status": "fail",
                "data": {
                    "error": "The extension of the provided file could not be determined.",
                    "supported_media_types": supported_formats
                }
            }), BAD_REQUEST

        # send UNSUPPORTED_MEDIA_TYPE when unsupported extension was provided
        if filename_parts[-1] not in supported_formats:
            return jsonify({
                "status": "fail",
                "data": {
                    "error": "The offered media type was not supported.",
                    "supported_media_types": supported_formats
                }
            }), UNSUPPORTED_MEDIA_TYPE

        # get file extension
        file_extension = filename_parts[-1]

        # initiate config variables with defaults
        encoding = enums.RecognitionConfig.AudioEncoding.ENCODING_UNSPECIFIED
        sample_rate_hertz = 16000

        # initiate audio bytes variable
        audio_bytes = None

        if file_extension == 'ogg':

            # read OGG file
            ogg_file = AudioSegment.from_ogg(audio_file)

            # instantiate audio_bytes with raw data
            audio_bytes = ogg_file.raw_data

            # specify encoding
            encoding = enums.RecognitionConfig.AudioEncoding.LINEAR16

            # specify sample rate
            sample_rate_hertz = ogg_file.frame_rate

        if file_extension == 'wav':

            # read WAV file
            wav_file = AudioSegment.from_wav(audio_file)

            # instantiate audio_bytes with raw data
            audio_bytes = wav_file.raw_data

            # specify encoding
            encoding = enums.RecognitionConfig.AudioEncoding.LINEAR16

            # specify sample rate
            sample_rate_hertz = wav_file.frame_rate

        # Cast data to appropriate google speech cloud types
        audio = types.RecognitionAudio(content=audio_bytes)
        config = types.RecognitionConfig(
            encoding=encoding,
            sample_rate_hertz=sample_rate_hertz,
            language_code=language_code)

        # recognize on the google speech cloud
        client_response = client.recognize(config, audio)

        # filter top results for every identifiable chunk of speech
        transcript = [str(result.alternatives[0].transcript) for result in client_response.results]

        # instantiate the response
        response = jsonify({
            "status": "success",
            "data": {
                "transcript": transcript
            }
        })
        return response, OK

    except Exception as e:

        # return error if fails. Never send exception error message in production!
        return jsonify({
            "status": "fail",
            "data": {
                "error": "An internal server error occurred.",
                "message": str(e)
            }
        }), INTERNAL_SERVER_ERROR


app.run(host='0.0.0.0')
